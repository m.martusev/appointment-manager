<?php

class Appointment
{

    private $id;
    private $name;
    private $email;
    private $phone;
    private $nationalId;
    private $date;
    private $time;

    public function __construct($id, $name, $email, $phone, $nationalId, $date)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->nationalId = $nationalId;
        $this->date = $date;
    }

    public function toString()
    {
        $string = "id: $this->id | name: $this->name | email: $this->email | phone: $this->phone | national ID: $this->nationalId | date and time: $this->date";
        return $string;
    }

    public function toCsv()
    {
        $string = "$this->id,$this->name,$this->email,$this->phone,$this->nationalId,$this->date";
        return $string;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function setNationalId($nationalId)
    {
        $this->nationalId = $nationalId;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public static function compareByTime($a, $b)
    {
        if (strtotime($a->getDate()) == strtotime($b->getDate())) {
            return 0;
        }
        return strtotime($a->getDate()) < strtotime($b->getDate()) ? -1 : 1;
    }
}
