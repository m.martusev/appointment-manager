<?php

class Validator
{

    public static function validateEmail($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function validatePhone($phone)
    {
        if (strlen($phone) == 12) {
            if (substr($phone, 0, 5) == "+3706" && is_numeric(substr($phone, 5, 7))) #if string starts with "+3706" and the remaining symbols are numbers
            {
                return true;
            }
        } elseif (strlen($phone) == 9) {
            if (substr($phone, 0, 2) == "86" && is_numeric(substr($phone, 2, 7))) #if string starts with "86" and the remaining symbols are numbers
            {
                return true;
            }

        }
        return false;
    }

    public static function validateNationalId($nationalId)
    {
        if (!is_numeric($nationalId)) {
            return false;
        }

        if (strlen($nationalId) != 11) {
            return false;
        }

        if ($nationalId[0] > 6) {
            return false;
        }
        return true;
    }

    public static function validateTime($time, $format = "Y-m-d H:i")
    {
        $t = DateTime::createFromFormat($format, $time);
        return $t && $t->format($format) == $time;
    }
}
