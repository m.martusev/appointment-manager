<?php
include "Appointment.php";
include "Validator.php";

class AppointmentController
{
    const ID_FILE_NAME = "id.txt";
    private $appointmentList = array();
    private $csvFileName;

    public function __construct($csvFileName) #load appointments from csv file

    {
        $this->csvFileName = $csvFileName;
        $csvFile = fopen($csvFileName, "r") or exit("Unable to open file " . $csvFileName);
        while (($line = fgetcsv($csvFile, 254, ",")) !== false) {
            $appointment = new Appointment($line[0], $line[1], $line[2], $line[3], $line[4], $line[5]);
            $this->appointmentList[$appointment->getId()] = $appointment;
        }
        fclose($csvFile);
    }

    public function addAppointment()
    {
        $info = $this->fillAppointmentInfo();

        $idFile = fopen(self::ID_FILE_NAME, "r"); //get id for appointment
        $id = fread($idFile, filesize(self::ID_FILE_NAME));
        fclose($idFile);

        $appointment = new Appointment($id, $info["name"], $info["email"], $info["phone"],
            $info["nationalId"], $info["date"]);
        $this->appointmentList[$id] = $appointment;

        $idFile = fopen(self::ID_FILE_NAME, "w"); //update following id number
        fwrite($idFile, $id + 1);
        fclose($idFile);
        echo ("Appointment added\n");
    }

    public function editAppointment()
    {
        echo ("Enter ID of the appointment to edit\n");
        $id = trim(fgets(STDIN, 10));
        if ($id == "/") {
            throw new Exception("User cancelled action.\n");
        }

        $appointment = $this->appointmentList[$id];
        if ($appointment == null) {
            throw new Exception("Appointment not found\n");
        }

        $info = $this->fillAppointmentInfo();

        $appointment->setName($info["name"]);
        $appointment->setEmail($info["email"]);
        $appointment->setPhone($info["phone"]);
        $appointment->setNationalId($info["nationalId"]);
        $appointment->setDate($info["date"]);

        echo ("Appointment updated\n");
    }

    public function deleteAppointment()
    {
        echo ("Enter ID of the appointment to delete\n");
        $id = trim(fgets(STDIN, 10));
        if ($id == "/") {
            throw new Exception("User cancelled action.\n");
        }

        if ($this->appointmentList[$id] == null) {
            throw new Exception("Appointment not found\n");
        }

        unset($this->appointmentList[$id]);
        echo ("Appointment removed\n");
    }

    public function printAppointmentsByDate()
    {
        echo ("Enter date (yyyy-mm-dd format)\n");
        $date = trim(fgets(STDIN, 16));

        if ($date == "/") {
            throw new Exception("User cancelled action.\n");
        }

        $results = array();
        foreach ($this->appointmentList as $appointment) {
            if (date('Y-m-d', strtotime($appointment->getDate())) == $date) {
                $results[] = $appointment;
            }
        }
        usort($results, array("Appointment", "compareByTime")); //sort by time

        echo ("List of appointments for $date\n");
        foreach ($results as $appointment) {
            echo ($appointment->toString() . "\n");
        }
    }

    public function printAll()
    {
        foreach ($this->appointmentList as $appointment) {
            echo ($appointment->toString() . "\n");
        }
    }

    public function saveToCsv()
    {
        $csvFile = fopen($this->csvFileName, "w");
        foreach ($this->appointmentList as $appointment) {
            fwrite($csvFile, $appointment->toCsv() . "\n");
        }
        fclose($csvFile);
    }

    public function fillAppointmentInfo()
    {
        $info = array();

        echo ("Enter name\n");
        $input = trim(fgets(STDIN, 64));
        if ($input == "/") {
            throw new Exception("User cancelled action.\n");
        }

        $info["name"] = $input;

        echo ("Enter email\n");
        while (true) {
            $input = trim(fgets(STDIN, 64));
            if ($input == "/") {
                throw new Exception("User cancelled action.\n");
            }

            if (!Validator::validateEmail($input)) {
                echo ("Invalid email\n");
            } else {
                $info["email"] = $input;
                break;
            }
        }

        echo ("Enter phone number\n");
        while (true) {
            $input = trim(fgets(STDIN, 64));
            if ($input == "/") {
                throw new Exception("User cancelled action.\n");
            }

            if (!Validator::validatePhone($input)) {
                echo ("Invalid phone number\n");
            } else {
                $info["phone"] = $input;
                break;
            }
        }

        echo ("Enter national identification number\n");
        while (true) {
            $input = trim(fgets(STDIN, 64));
            if ($input == "/") {
                throw new Exception("User cancelled action.\n");
            }

            if (!Validator::validateNationalId($input)) {
                echo ("Invalid national ID number\n");
            } else {
                $info["nationalId"] = $input;
                break;
            }
        }

        echo ("Enter date and time (yyyy-mm-dd hh:mm format)\n");
        while (true) {
            $input = trim(fgets(STDIN, 64));
            if ($input == "/") {
                throw new Exception("User cancelled action.\n");
            }

            if (!Validator::validateTime($input)) {
                echo ("Invalid time\n");
            } else {
                $info["date"] = $input;
                break;
            }
        }
        return $info;
    }
}
