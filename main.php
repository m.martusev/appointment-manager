<?php
include "AppointmentController.php";

class Main
{
    private $appointmentController;

    public function run()
    {
        global $argv;
        $appointmentController = new AppointmentController($argv[1]); #pass csv file name

        do {
            $this->displayActions();
            $userInput = trim(fgets(STDIN, 4));
            try {
                switch ($userInput) {
                    case 1:
                        $appointmentController->addAppointment();
                        break;
                    case 2:
                        $appointmentController->printAll();
                        $appointmentController->editAppointment();
                        break;
                    case 3:
                        $appointmentController->printAll();
                        $appointmentController->deleteAppointment();
                        break;
                    case 4:
                        $appointmentController->printAppointmentsByDate();
                        break;
                    case 5:
                        $appointmentController->saveToCsv();
                        break;
                    default:
                        echo ("Action not found\n");
                }
            } catch (Exception $e) {
                echo ($e->getMessage());
            }
        } while ($userInput != 5);
    }

    private function displayActions()
    {
        echo ("--Please select an action--\n");
        echo ("[1] Add appointment\n");
        echo ("[2] Edit appointment\n");
        echo ("[3] Delete appointment\n");
        echo ("[4] Print list of appointments for specific date\n");
        echo ("[5] Exit\n");
    }

}
$program = new Main();
$program->run();
