## Instructions
To run the program, run main.php and specify the csv file you want to load data from.
```
php main.php appointments.csv
```
* In the main menu, type the number corresponding to the action you wish to perform.
* After choosing an action, you can type '/' at any time to cancel that action and return to the main menu.
* After choosing to exit the program, it will save all the changes to the same csv file that the data was loaded from.
